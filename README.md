# GB Mail

## Description

GB Mail is a simple tool which uses sqlite and email templates in the form of html files to send emails to a list of recipients.
Sent emails are recorded internally, so that the same email cannot be sent twice.

Reason for this small app is to send a bunch of well customised emails without need of manual changes to them.

## How to build it

GB Mail is a simple GO app, so it can be built with the following command:
```shell
make build
```
The resulting app will be placed in the `bin` directory. It is self-contained and does not require any external dependencies. The user is free to place it wherever they want. If the location is in the PATH, then it can be run from anywhere.

## How to use it

Currently the app has two modes:
1. `send` - sends emails to a list of recipients.
1. `init` - initialises the database in `./database.db` and the template directory in `./templates`.

To use the app, run the following command:
1. `gb_email -mode=init`
1. Open the sqlite file created in the app folder with e.g. `sqlitebrowser`:
    1. add all recipients.
    1. add the email templates. The value of `subject` field will be used in the email. The value of `path` is relative to `templates` folder, e.g.:
    If template file is located in `templates/main.html` the value of the path field should be `main.html`.
1. Add template files to the `templates` folder. The app uses build in golang template engine, so two variables can be used with the templates:
    1. `{{.Email}}` - the email address of the recipient.
    1. `{{.Name}}` - the name of the recipient.
1. Configure sender account. To do that use environment variables, for safety concerns. The variables are:
    1. `EMAIL_SMTP_HOST` - SMTP server address.
    1. `EMAIL_SMTP_PORT` - SMTP server port.
    1. `EMAIL_SMTP_USERNAME` - SMTP server username.
    1. `EMAIL_SMTP_PASSWORD` - SMTP server password.
1. Run the app: `gb_email -mode=send`

**NOTE:** The app tracks which emails have been sent to prevent duplicate emails. To audit that see `send_operations` table in the sqlite DB.