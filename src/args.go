package main

import (
	"flag"
	"log"

	"gitlab.com/matsto/gb_email/src/modes"
)

type Args struct {
	mode     modes.Mode
	template string
}

func parseArgs() *Args {
	var (
		mode     modes.Mode
		template string
	)
	const (
		modeUsage     = "Application mode: init or send"
		templateUsage = "A template to send"
	)

	flag.StringVar(&mode, "mode", "", modeUsage)
	flag.StringVar(&template, "template", "", templateUsage)

	// Short versions of flags
	flag.StringVar(&mode, "m", "", modeUsage)
	flag.StringVar(&template, "t", "", templateUsage)

	flag.Parse()

	if mode != modes.InitMode && mode != modes.SendMode {
		log.Fatalln("Error: Invalid mode. Please use either init or send.")
	}
	return &Args{mode, template}
}
