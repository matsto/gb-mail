package modes

type Mode = string

const (
	InitMode Mode = "init"
	SendMode Mode = "send"
)
