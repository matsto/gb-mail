package modes

import (
	"bytes"
	"fmt"
	"log"
	"net/smtp"
	"text/template"

	"gitlab.com/matsto/gb_email/src/database"
	"gitlab.com/matsto/gb_email/src/email"
)

const mimeHeaders = "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"

func SendEmails(templateName string) {
	emailConfig := email.NewEmailSMTPConfig()
	auth := email.LoginAuth(
		emailConfig.Username,
		emailConfig.Password,
	)
	// Query recipients from the database
	var (
		templates []database.Template
		err error
	)
	if templateName == "" {
		templates, err = database.SelectAllTemplates()
	} else {
		temp, _err := database.SelectOneTemplate(templateName)
		err = _err
		templates = append(templates, temp)

	}

	if err != nil {
		log.Fatal(err)
	}
	for _, _template := range templates {
		log.Printf("Template: %v\n", _template.Name)
		recipients, err := database.SelectRecipientsForTemplate(_template.Id)
		if err != nil {
			log.Fatal(err)
		}
		tmpl, err := template.ParseFiles("templates/" + _template.Path)
		if err != nil {
			log.Fatal(err)
		}

		for _, recipient := range recipients {
			log.Printf("Recipient: %v\n", recipient.Email)
			var buf bytes.Buffer
			_, err := buf.Write([]byte(fmt.Sprintf("Subject: %s \n%s\n\n", _template.Subject, mimeHeaders)))
			if err != nil {
				log.Fatal(err)
			}

			err = tmpl.Execute(&buf, recipient)
			if err != nil {
				log.Fatal(err)
			}

			err = smtp.SendMail(fmt.Sprintf("%s:%s", emailConfig.Host, emailConfig.Port), auth, emailConfig.Username, []string{recipient.Email}, buf.Bytes())
			if err != nil {
				log.Fatalln(err)
			}

			op := database.SendOperation{
				TemplateId:  _template.Id,
				RecipientId: recipient.Id,
			}
			err = op.Insert()
			if err != nil {
				log.Fatalln(err)
			}
		}
	}
}
