package modes

import (
	"os"

	"gitlab.com/matsto/gb_email/src/database"
)

func createTemplateFolder() {
	const templateFolder = "templates"
	// Create template folder
	if _, err := os.Stat(templateFolder); os.IsNotExist(err) {
		os.Mkdir(templateFolder, 0755)
	}
}

func initializeTables() {
	tables := []database.Table{
		&database.Recipient{},
		&database.Template{},
		&database.SendOperation{},
	}
	for _, table := range tables {
		table.CreateTable()
	}

}

func Initialize() {
	createTemplateFolder()
	initializeTables()
}
