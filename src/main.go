package main

import (
	"log"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/matsto/gb_email/src/database"
	"gitlab.com/matsto/gb_email/src/modes"
)

func main() {
	// Open SQLite database
	args := parseArgs()
	err := database.Init()
	if err != nil {
		log.Fatal(err)
	}
	defer database.DB.Close()

	// Switch between modes
	switch args.mode {
	case modes.InitMode:
		modes.Initialize()
	case modes.SendMode:
		modes.SendEmails(args.template)
	default:
		log.Fatalln("Error: Invalid mode. Use 'init' or 'send'.")
	}

}
