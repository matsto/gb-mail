package database

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

var DB *sql.DB

func Init() error {
	var err error
	DB, err = sql.Open("sqlite3", "database.db")
	return err
}

type Table interface {
	CreateTable()
	DropTable()
}
