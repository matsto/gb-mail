package database

import "log"

type Template struct {
	Id      int    `db:"id"`
	Name    string `db:"name"`
	Subject string `db:"subject"`
	Path    string `db:"path"`
}

func (t *Template) CreateTable() {
	_, err := DB.Exec(`
	CREATE TABLE IF NOT EXISTS templates(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		name TEXT NOT NULL UNIQUE,
		subject TEXT NOT NULL,
		path TEXT NOT NULL UNIQUE
	)`)
	if err != nil {
		log.Fatal(err)
	}
}

func (t *Template) DropTable() {
	_, err := DB.Exec("DROP TABLE IF EXISTS templates")
	if err != nil {
		log.Fatal(err)
	}
}

func SelectAllTemplates() ([]Template, error) {
	rows, err := DB.Query("SELECT id, name, subject, path FROM templates")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var templates []Template
	for rows.Next() {
		var temp Template
		if err := rows.Scan(&temp.Id, &temp.Name, &temp.Subject, &temp.Path); err != nil {
			return templates, err
		}
		templates = append(templates, temp)
	}
	if err = rows.Err(); err != nil {
		return templates, err
	}
	return templates, nil
}

func SelectOneTemplate(name string) (Template, error) {
	var temp Template
	row := DB.QueryRow("SELECT id, name, subject, path FROM templates WHERE name =?", name)
	err := row.Scan(&temp.Id, &temp.Name, &temp.Subject, &temp.Path)
	if err != nil {
		return temp, err
	}
	return temp, nil
}
