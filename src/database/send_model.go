package database

import "log"

type SendOperation struct {
	Id          int    `db:"id"`
	RecipientId int    `db:"recipient_id"`
	TemplateId  int    `db:"template_id"`
	SentAt      string `db:"sent_at"`
}

func (s *SendOperation) CreateTable() {
	_, err := DB.Exec(`
	CREATE TABLE IF NOT EXISTS send_operations(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		recipient_id INTEGER NOT NULL,
		template_id INTEGER NOT NULL,
		sent_at TEXT DEFAULT CURRENT_TIMESTAMP NOT NULL,
		FOREIGN KEY (recipient_id) REFERENCES recipients (id), 
		FOREIGN KEY (template_id) REFERENCES templates (id),
		UNIQUE(recipient_id, template_id)
	)`)
	if err != nil {
		log.Fatal(err)
	}
}

func (s *SendOperation) DropTable() {
	_, err := DB.Exec("DROP TABLE IF EXISTS send_operations")
	if err != nil {
		log.Fatal(err)
	}
}

func (s *SendOperation) Insert() error {
	_, err := DB.Exec(`
	INSERT INTO send_operations(recipient_id, template_id)
	VALUES(?,?)
	`, s.RecipientId, s.TemplateId)
	return err
}
