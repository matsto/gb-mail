package database

import "log"

type Recipient struct {
	Id    int    `db:"id"`
	Name  string `db:"name"`
	Email string `db:"email"`
}

func (r *Recipient) CreateTable() {
	_, err := DB.Exec(`
	CREATE TABLE IF NOT EXISTS recipients(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		name TEXT NOT NULL UNIQUE,
		email TEXT NOT NULL UNIQUE
	)`)
	if err != nil {
		log.Fatal(err)
	}
}

func (r *Recipient) DropTable() {
	_, err := DB.Exec("DROP TABLE IF EXISTS recipients")
	if err != nil {
		log.Fatal(err)
	}
}

func SelectAllRecipients() ([]Recipient, error) {
	rows, err := DB.Query("SELECT id, name, email FROM recipients")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var recipients []Recipient

	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var rec Recipient
		if err := rows.Scan(&rec.Id, &rec.Name, &rec.Email); err != nil {
			return recipients, err
		}
		recipients = append(recipients, rec)
	}
	if err = rows.Err(); err != nil {
		return recipients, err
	}
	return recipients, nil

}

func SelectRecipient(id string) (Recipient, error) {
	var recipient Recipient
	row := DB.QueryRow("SELECT id, name, email FROM recipients WHERE id=?", id)
	err := row.Scan(&recipient.Id, &recipient.Name, &recipient.Email)
	if err != nil {
		return recipient, err
	}
	return recipient, nil
}

func SelectRecipientsForTemplate(templateId int) ([]Recipient, error) {
	rows, err := DB.Query(`
	SELECT id, name, email
	FROM recipients
	WHERE id NOT IN (
		SELECT recipient_id
		FROM send_operations
		WHERE template_id =?
	)`, templateId)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var recipients []Recipient

	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var rec Recipient
		if err := rows.Scan(&rec.Id, &rec.Name, &rec.Email); err != nil {
			return recipients, err
		}
		recipients = append(recipients, rec)
	}
	if err = rows.Err(); err != nil {
		return recipients, err
	}
	return recipients, nil

}
