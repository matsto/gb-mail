package email

import (
	"errors"
	"log"
	"net/smtp"
	"os"
)

type EmailSMTPConfig struct {
	Host     string
	Port     string
	Username string
	Password string
}

type loginAuth struct {
	username, password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("unknown fromServer")
		}
	}
	return nil, nil
}

func NewEmailSMTPConfig() *EmailSMTPConfig {
	var emailSMTPConfig EmailSMTPConfig

	host, exists := os.LookupEnv("EMAIL_SMTP_HOST")
	if !exists {
		log.Fatalln("Error: EMAIL_SMTP_HOST environment variable is not set.")
	}
	emailSMTPConfig.Host = host

	port, exists := os.LookupEnv("EMAIL_SMTP_PORT")
	if !exists {
		log.Fatalln("Error: EMAIL_SMTP_PORT environment variable is not set.")
	}
	emailSMTPConfig.Port = port

	username, exists := os.LookupEnv("EMAIL_SMTP_USERNAME")
	if !exists {
		log.Fatalln("Error: EMAIL_SMTP_USERNAME environment variable is not set.")
	}
	emailSMTPConfig.Username = username

	password, exists := os.LookupEnv("EMAIL_SMTP_PASSWORD")
	if !exists {
		log.Fatalln("Error: EMAIL_SMTP_PASSWORD environment variable is not set.")
	}
	emailSMTPConfig.Password = password

	return &emailSMTPConfig
}
